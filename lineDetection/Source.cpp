#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <iostream>
#include "Source.h"

using namespace cv;
using namespace std;


int main(int argc, char** argv)
{
    // Declare the output variables
    Mat dst, cdst, cdstP;
    const char* default_file = "../road.png";
    const char* filename = argc >= 2 ? argv[1] : default_file;

    
    
    // Loads an image
    Mat src = imread(samples::findFile(filename), IMREAD_COLOR);
    
    // Check if image is loaded fine
    if (src.empty()) {
        printf(" Error opening image\n");
        printf(" Program Arguments: [image_name -- default %s] \n", default_file);
        return -1;
    }

    imshow("Source", src);

    

    int height = src.rows, width = src.cols;
    cout << width<< " "<< height;

    //int lineType = LINE_8;
    //Point trapez[1][4];
    //trapez[0][0] = Point(width/2 + 100,height/2);
    //trapez[0][1] = Point(width / 2 - 100, height / 2);
    //trapez[0][2] = Point(width / 2 - 550, height);
    //trapez[0][3] = Point(width / 2 + 550, height);

    //const Point* ppt[1] = { trapez[0] };
    //int npt[] = { 4 };

    //Mat src_poly = src;
    ////Daws a polyghon
    //fillPoly(src_poly, ppt, npt, 1, Scalar(255, 255, 255), lineType);

    //imshow("ROI", src_poly);

    Point2f trapez1[4];
    trapez1[0] = Point(width / 2 + 40, height / 2 + 20);
    trapez1[1] = Point(width / 2 - 70, height / 2 + 20);
    trapez1[2] = Point(width / 2 - 230, height);
    trapez1[3] = Point(width / 2 + 180, height);

    Point2f birdView[4];
    birdView[0] = Point(400,0);
    birdView[1] = Point(0,0);
    birdView[2] = Point(0, 400);
    birdView[3] = Point(400, 400);

    //Prepare matrix for transform and get the warped image
    Mat perspectiveMatrix = getPerspectiveTransform(trapez1, birdView);
    Mat ROI(400, 400, CV_8UC3); //Destination for warped image

    Mat invertedPerspectiveMatrix;
    invert(perspectiveMatrix, invertedPerspectiveMatrix);

    Mat org; //Original image, modified only with result
    Mat img; //Working image

   //Generate bird's eye view
    warpPerspective(src, ROI, perspectiveMatrix, ROI.size(), INTER_LINEAR, BORDER_CONSTANT);

   // imshow("ROI_persp", ROI_image);
    
    Mat ROI_gray;
    cvtColor(ROI, ROI_gray, COLOR_BGR2GRAY);
    imshow("Gray", ROI_gray);
    moveWindow("Gray", 0, 0);

    Mat ROI_blured;
    //Blur the image a bit so that gaps are smoother
    const Size kernelSize = Size(9, 9);
    GaussianBlur(ROI_gray, ROI_blured, kernelSize, 0);
    imshow("Blur", ROI_blured);
    moveWindow("Blur", 400, 0);

    //Keep only what's above 150 value, other is then black
    Mat ROI_binary;
    const int thresholdVal = 150;
    threshold(ROI_blured, ROI_binary, thresholdVal, 255, THRESH_BINARY);
    imshow("Binary", ROI_binary);
    moveWindow("Binary", 800, 0);

    // Find the edges in the image using canny detector
    Mat ROI_edges;
    Canny(ROI_binary, ROI_edges, 50, 200);
    imshow("Canny", ROI_edges);
    moveWindow("Canny", 1200, 0);

    // Create a vector to store lines of the image
    //vector<Vec4i> lines;
    //int thresh = 150;
    //// Apply Hough Transform
    //HoughLinesP(ROI_edges, lines, 1, CV_PI / 180, thresh, 5, 250);

    //// Draw lines on the image
    //for (size_t i = 0; i < lines.size(); i++) {
    //    Vec4i l = lines[i];
    //    line(ROI, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255, 0, 0), 3, LINE_AA);
    //}
    // Show result image
    //imshow("Result Image", ROI);

    // Create a window
    namedWindow("Edge trackbar", 1);

    //Create trackbar to change brightness
    int iThreshold = 50;
    createTrackbar("Threshold", "Edge trackbar", &iThreshold, 255);

    int iMinLength = 50;
    createTrackbar("MinLength", "Edge trackbar", &iMinLength, 255);

    int iMaxLength = 50;
    createTrackbar("MaxLength", "Edge trackbar", &iMaxLength, 255);

   
    
    vector<Vec4i> lines;
    // Declare calibrated values for the wanted variables
    // Threshold = 40; MinLength = 0; MaxLength = 255
    

    while (true)
    {
        Mat display = ROI.clone();
        int threshold = iThreshold;
        int minLength = iMinLength;
        int maxLength = iMaxLength;

        HoughLinesP(ROI_edges, lines, 1, CV_PI / 180, threshold, minLength, maxLength);

        // Draw lines on the image
        for (size_t i = 0; i < lines.size(); i++) {
            Vec4i l = lines[i];
            line(display, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255, 0, 0), 3, LINE_AA);
        }

        auto halfHeight = display.size().height / 2;
        auto halfWidth = display.size().width / 2;

        

        // Am punctul de pe dreapta centrat pe mijloc

        // Cautare la stanga

        auto leftMargin = Point(0, 0);
        
        for (MatIterator_<Vec3b> it = display.begin<Vec3b>() + halfHeight * (display.size().width) + halfWidth; it != display.begin<Vec3b>() + halfHeight * (display.size().width); it--)
        {   
            if ((*it)[0] == 255)
            {
                leftMargin = it.pos(); 
                break;
            }
        }

        // Cautare la dreapta

        auto rightMargin = Point(0, 0);
        for (MatIterator_<Vec3b> it = display.begin<Vec3b>() + halfHeight * (display.size().width) + halfWidth; it != display.begin<Vec3b>() + (halfHeight + 1) * (display.size().width) - 1; it++)
        {
            if ((*it)[0] == 255)
            {
                rightMargin = it.pos();
                break;
            }
        } 

        //perspectiveTransform(display, src, invertedPerspectiveMatrix); //Transform points back into original image space

        //show the brightness and contrast adjusted image

        line(display, Point(0, display.size().height / 2), Point(display.size().width, display.size().height / 2), Scalar(0, 255, 0), 1, LINE_AA);
        line(display, Point(display.size().width / 2, display.size().height / 2 - 10), Point(display.size().width / 2, display.size().height / 2 + 10), Scalar(0, 255, 0), 1, LINE_AA);
        
        if (leftMargin.y == rightMargin.y)
        {
            line(display, leftMargin, rightMargin, Scalar(0, 0, 255), 1, LINE_AA);
            auto centerPoint = Point((rightMargin.x + leftMargin.x) / 2, halfHeight);

            line(display, Point((rightMargin.x + leftMargin.x) / 2, halfHeight - 10), Point((rightMargin.x + leftMargin.x) / 2, halfHeight + 10), Scalar(0, 0, 255), 1, LINE_AA);
            
            if (centerPoint.x < halfWidth)
            {
                putText(display, "Steer left", Point(halfWidth - 20, halfHeight + 30), FONT_HERSHEY_DUPLEX, 0.5, Scalar(255, 255, 255), 1);
            }
            else if (centerPoint.x > halfWidth)
            {
                putText(display, "Steer right", Point(halfWidth - 20, halfHeight + 30), FONT_HERSHEY_DUPLEX, 0.5, Scalar(255, 255, 255), 1);
            }
        }
        

        imshow("Edge trackbar", display);

        // Wait until user press some key for 50ms
        int iKey = waitKey(50);

        //if user press 'ESC' key
        if (iKey == 27)
        {
            break;
        }
    }

    waitKey();
    return 0;
}